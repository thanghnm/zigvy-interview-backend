// Import express
const express = require("express")
// Import Mongoose
const mongoose = require("mongoose")
const userRouter = require("./app/routers/user.router");
const albumRouter = require("./app/routers/album.router");
const photoRouter = require("./app/routers/photo.router");
const todoRouter = require("./app/routers/todo.router");
const postRouter = require("./app/routers/post.router");
const commentRouter = require("./app/routers/comment.router");

//B2: Khởi tạo app express
const app = new express()
app.use(express.json())
const port = 8000

// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/Zigvy")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleEror(error))

app.use("/users", userRouter);
app.use("/albums", albumRouter);
app.use("/photos", photoRouter);
app.use("/todos", todoRouter);
app.use("/posts", postRouter);
app.use("/comments", commentRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})