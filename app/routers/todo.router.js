// Import express
const express = require("express");
const mongoose = require("mongoose");
const { getAllTodos, createTodo, getTodoById, updateTodoById, deleteTodoById } = require("../controllers/todo.controller");

const router = express.Router();
router.get("/", getAllTodos);

router.post("/", createTodo)

router.get("/:albumId", getTodoById)

router.put("/:albumId", updateTodoById)

router.delete("/:albumId", deleteTodoById)

module.exports = router;