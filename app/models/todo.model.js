//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema

const todoSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref:"User"
    },
    id: {
        type: Number,
        required: true,
        unique:true,
    },
    title: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        required: true
    }
});
module.exports = mongoose.model('Todo', todoSchema);
