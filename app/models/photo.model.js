//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema

const photoSchema = new Schema({
    albumId: {
        type: Schema.Types.ObjectId,
        ref:"Album"
    },
    id: {
        type: Number,
        required: true,
        unique:true,
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model('Photo', photoSchema);
