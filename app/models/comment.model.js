const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const commentSchema = new Schema({
    id: {type: Number,
        required: true,
        unique:true
    },
    postId: {
        type: mongoose.Types.ObjectId,
        ref: "Post"
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Comment', commentSchema);
