//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
//  Khởi tạo schema
const postSchema = new Schema({
    

    userId:{
        type: Schema.Types.ObjectId,
        ref:"User"
    },
    id:{
        type:Number,
        required:true,
        unique:true

    },
    title:{
        type:String,
        required:true
    },
    body:{
        type:String,
        required:true,
    }

    
})
// Biên dịch Schema
module.exports = mongoose.model("Post",postSchema)