//Import Model
const userModel = require("../models/user.model")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, name, username, website, phone,street,suite,city,zipcode,companyname,catchPhrase,bs,lat,lng } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    if (!username) {
        return res.status(400).json({
            status: "Bad request",
            message: "username is required"
        })
    }
    if (!website) {
        return res.status(400).json({
            status: "Bad request",
            message: "website is required"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }

    // B3 Xử lý 
    let newUser = {
        _id:new mongoose.Types.ObjectId,
        id:id,
        name:name,
        address:{
            zipcode,
            street,
            suite,
            city,
            geo:{
                lat,
                lng
            }
        },
        phone:phone,
        username,
        website,
        company:{
            name:companyname,
            catchPhrase,
            bs
        }
    }
    try {
        const result = await userModel.create(newUser);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Drink
const getAllUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.find({
    });
    return res.status(200).json({
        result:result.album
    });

}
// Hàm lấy dữ liệu Drink bằng Id
const getUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await userModel.findById(userId);
    return res.status(200).json({
        result
    });
}
// Hàm Update Drink bằng Id
const updateUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    const { id, name, username, website, phone,street,suite,city,zipcode,companyname,catchPhrase,bs,lat,lng } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedUser = {
        id:id,
        name:name,
        address:{
            zipcode,
            street,
            suite,
            city,
            geo:{
                lat,
                lng
            }
        },
        phone:phone,
        username,
        website,
        company:{
            name:companyname,
            catchPhrase,
            bs
        }}
    const result = await userModel.findByIdAndUpdate(userId, updatedUser);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });
}
// Hàm xóa Drink bằng Id
const deleteUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.findByIdAndDelete(userId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = { createUser  ,getAllUser, getUserById , updateUserById , deleteUserById
}