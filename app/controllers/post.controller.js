//Import Model
const postModel = require("../models/post.model")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createPost = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, title, userId, body } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required"
        })
    }
    if (!userId) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is required"
        })
    }
    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required"
        })
    }

    // B3 Xử lý 
    let newObj = {
        id,
        userId,
        title,
        body
    }
    try {
        const result = await postModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Drink
const getAllPost = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.query.userId
    // console.log(userId)
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await postModel.find(condition);
    return res.status(200).json({
        result: result
    });

}
// Hàm Lấy dữ liệu tất cả Album
const getAllPostOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.params.userId
    if (userId) {


        // B2 Kiểm tra dữ liệu
        // B3 Xử lý 
        const result = await postModel.find({
            userId: userId
        });
        return res.status(200).json({
            result
        });
    }
}
// Hàm lấy dữ liệu Drink bằng Id
const getPostById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var postId = req.params.postId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Post Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await postModel.findById(postId);
    return res.status(200).json({
        result
    });
}
// Hàm Update Drink bằng Id
const updatePostById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var postId = req.params.postId
    const { id, title, userId, body } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedUser = {
        id,
        userId,
        title,
        body
    }
    const result = await postModel.findByIdAndUpdate(postId, updatedUser);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });
}
// Hàm xóa Drink bằng Id
const deletePostById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var postId = req.params.postId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await postModel.findByIdAndDelete(postId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = {
    createPost, getAllPost, getPostById, updatePostById, deletePostById, getAllPostOfUser
}