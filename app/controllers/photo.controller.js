// Import Model
const photoModel = require("../models/photo.model")
const mongoose = require("mongoose")

// Hàm tạo Drink
const createPhoto = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { albumId, id,title,url } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!albumId) {
        return res.status(400).json({
            status: "Bad request",
            message: "albumId is required"
        })
    }
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required"
        })
    }
    if (!url) {
        return res.status(400).json({
            status: "Bad request",
            message: "url is required"
        })
    }

    
    // B3 Xử lý 
    let newObj = {
        id,
        albumId,
        title,
        url
    }
    try {
        const result = await photoModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Album
const getAllPhoto = async (req, res) => {
    // B1 Thu thập dữ liệu
    const albumId = req.query.albumId
    // console.log(userId)
    let condition = {};
    if (albumId) {
        condition.albumId = albumId;        
    }

    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await photoModel.find(condition);
    return res.status(200).json({
        result
    });

}
// Hàm Lấy dữ liệu tất cả Album
const getAllPhotoOfAlbum = async (req, res) => {
    // B1 Thu thập dữ liệu
    const albumId = req.params.albumId
    if(albumId){

    
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await photoModel.find({
        albumId:albumId
    });
    return res.status(200).json({
        result
    });
    }
}
// Hàm lấy dữ liệu Album bằng Id
const getPhotoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var photoId = req.params.photoId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await photoModel.findById(photoId);
    return res.status(200).json({
        result
    });

}
// Hàm Update Album bằng Id
const updatePhotoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var albumId = req.params.albumId
    const { id, photoId,url,title } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "albumId is invalid"
        })
    }
    // B3 Xử lý 
    let update = {
        id,
        photoId,
        title,
        url
    }
    const result = await photoModel.findByIdAndUpdate(albumId, update);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });

}
// Hàm xóa Drink bằng Id
const deletePhotoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var photoId = req.params.photoId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await photoModel.findByIdAndDelete(photoId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = { 
    createPhoto,getAllPhoto,getAllPhotoOfAlbum,getPhotoById,updatePhotoById,deletePhotoById
}