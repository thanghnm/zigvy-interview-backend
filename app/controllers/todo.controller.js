// Import Model
const todoModel = require("../models/todo.model")
const mongoose = require("mongoose")

// Hàm tạo 
const createTodo = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, userId,title,completed } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!userId) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is required"
        })
    }
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required"
        })
    }
    if (completed==Boolean) {
        return res.status(400).json({
            status: "Bad request",
            message: "completed is Boolean"
        })
    }
    
    // B3 Xử lý 
    let newObj = {
        id,
        userId,
        title,
        completed
    }
    try {
        const result = await todoModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Album
const getAllTodos = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.query.userId
    let condition = {};
    if (userId) {
        condition.userId = userId;        
    }

    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await todoModel.find(condition);
    return res.status(200).json({
        result
    });

}
// Hàm Lấy dữ liệu tất cả Album
const getAllTodosOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.params.userId
    if(userId){

    
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await todoModel.find({
        userId:userId
    });
    return res.status(200).json({
        result
    });
    }
}
// Hàm lấy dữ liệu Album bằng Id
const getTodoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var todoId = req.params.todoId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await todoModel.findById(todoId);
    return res.status(200).json({
        result
    });

}
// Hàm Update Album bằng Id
const updateTodoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var todoId = req.params.todoId
    const { id, userId,title,completed } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "todoId is invalid"
        })
    }
    // B3 Xử lý 
    let update = {
        id,
        userId,
        title,
        completed
    }
    const result = await todoModel.findByIdAndUpdate(todoId, update);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });

}
// Hàm xóa Drink bằng Id
const deleteTodoById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var todoId = req.params.todoId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await todoModel.findByIdAndDelete(todoId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = {
    createTodo,getAllTodos,getAllTodosOfUser,getTodoById,updateTodoById,deleteTodoById
}